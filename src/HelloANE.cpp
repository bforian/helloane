#include "HelloANE.h"
#include <cstring>


FREObject hello(FREContext ctx, void *data, uint32_t argc, FREObject argv[]) {
    FREObject result;
    const char *str = "HELLO";
    FRENewObjectFromUTF8((uint32_t)strlen(str)+1, (const uint8_t *)str, &result);
    return result;
}

FRENamedFunction aneFunctions[] = {
    { (const uint8_t*) "hello", 0, hello }
};

void contextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctions, const FRENamedFunction** functions) {
    
    *numFunctions = sizeof( aneFunctions ) / sizeof( FRENamedFunction );
    *functions = aneFunctions;
 }

void contextFinalizer(FREContext ctx)
{
    return;
}

void initializer(void** extData, FREContextInitializer* ctxInitializer, FREContextFinalizer* ctxFinalizer) {
    *ctxInitializer = &contextInitializer;
    *ctxFinalizer = &contextFinalizer;
}


void finalizer(void* extData) {
    return;
}
