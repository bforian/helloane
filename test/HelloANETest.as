﻿package  {
	import flash.display.MovieClip;
	import fs.ane.HelloANE;
	import flash.text.TextField;
	
	public class HelloANETest extends MovieClip {
		
		private var _ane = new HelloANE();
		public var tx: TextField;

		public function HelloANETest() {
			tx.text = _ane.hello();
		}
	}
	
}
