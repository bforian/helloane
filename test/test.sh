#!/bin/sh

name="HelloANE";
testPath=$(dirname $0);

cd ${testPath}/extensions;
rm -rf ${name}ANE.ane;
mkdir ${name}ANE.ane;

cd ${testPath}/extensions/${name}ANE.ane;
unzip ../${name}.ane;


cd ${testPath};

"/AIRSDK/bin/adl" -profile extendedDesktop -extdir extensions ${name}Test-app.xml;
