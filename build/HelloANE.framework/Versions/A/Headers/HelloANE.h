
#include "FlashRuntimeExtensions.h"
#ifdef _WIN32
#define aneExport __declspec(dllexport)
#elif __APPLE__
#define aneExport __attribute__((visibility("default")))
#endif
extern "C" {

FREObject hello(FREContext ctx, void *data, uint32_t argc, FREObject argv[]);

void contextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctions, const FRENamedFunction** functions);
void contextFinalizer(FREContext ctx);
aneExport void initializer(void** extData, FREContextInitializer* ctxInitializer, FREContextFinalizer* ctxFinalizer);
aneExport void finalizer(void* extData);

}
