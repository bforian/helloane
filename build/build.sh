#!/bin/sh

name="HelloANE";

cd $(dirname $0);
echo "unzipping ${name}.swc";

JAR -xf ${name}.swc catalog.xml library.swf;

echo "building ${name}.ane";

"/AIRSDK/bin/adt" -package -target ane ${name}.ane ${name}.xml -swc ${name}.swc -platform MacOS-x86-64 library.swf ${name}.framework -platform Windows-x86 library.swf ${name}.dll;

echo "copying ${name}.ane to test folder";
cp -f ${name}.ane ../test/extensions/

